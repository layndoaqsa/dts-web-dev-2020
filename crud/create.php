<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Produk</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"></head>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('#btn-ulangi').click(function(){
                $("input[name=merek]").val('');
                $("input[name=warna]").val('');
                $("input[name=stok]").val('');
                $("input[name=satuan]").val('');
                $("input[name=harga]").val('');
            });
        });
    </script>
<body>
    <div class="container">
        <?php
            include "koneksi.php";

            // START cek apakah ada kiriman form dari method POST
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $merek  = $_POST["merek"];
                $warna  = $_POST["warna"];
                $stok   = $_POST["stok"];
                $satuan = $_POST["satuan"];
                $harga  = $_POST["harga"];
                $sql = "INSERT into produk (merek, warna, stok, satuan, harga) VALUES 
                       ('$merek','$warna','$stok','$satuan','$harga')"; 

                // START mengeksekusi data
                $hasil = mysqli_query($db,$sql);
                // END mengeksekusi data

                // START cek hasil eksekusi
                if ($hasil) {
                    header("Location:index.php");
                } else {
                    echo "<div class='alert alert-danger'> Data gagal disimpan. </div>";
                }
                // END cek hasil eksekusi
            }
            // END cek apakah ada kiriman form dari method POST

        ?>

        <h5>Tambah Produk</h5>
        <form action="create.php" method="post" id="form">
            <div class="form-group">
                <label for="merek">Merek</label>
                <input type="text" name="merek" placeholder="Masukkan merek" class="form-control" id="" aria-describedby="emailHelp" required>
            </div>
            <div class="form-group">
                <label for="warna">Warna</label>
                <input type="text" name="warna" placeholder="Masukkan warna" class="form-control" id="" required>
            </div>
            <div class="form-group">
                <label for="stok">Stok</label>
                <input type="number" name="stok" placeholder="Masukkan jumlah stok tersedia" class="form-control" id="" required>
            </div>
            <div class="form-group">
                <label for="satuan">Satuan</label>
                <input type="number" name="satuan" placeholder="Masukkan satuan produk" class="form-control" id="" required>
            </div>
            <div class="form-group">
                <label for="harga">Harga</label>
                <input type="number" name="harga" placeholder="Masukkan harga hanya angka" class="form-control" id="" required>
            </div>
            <a href="index.php" class="btn btn-warning"> Kembali</a>
            <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
            <button type="button" id="btn-ulangi">Reset</button>
            <button type="submit" placeholder="Masukkan merek" class="btn btn-primary">Simpan</button>
        </form>
    </div>
</body>
</html>